package com.example.davidc.myapplication


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.annotation.IntegerRes
import android.util.Log

import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    internal lateinit var yourScoreTextView: TextView
    internal lateinit var timeTextView: TextView
    internal lateinit var tapButton: Button
    internal var gameStarted = false
    internal var score = 0

    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCountDown = 60000L
    internal var countDownInterval = 1000L
    internal var timeLeft = 60
    internal var TAG = MainActivity::class.java.simpleName

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "onCreate called. Score is $score")

//connect views to variables

        yourScoreTextView = findViewById(R.id.gameScoreTextView)
        timeTextView = findViewById(R.id.gameTimeLeftTextView)
        tapButton = findViewById(R.id.tapButton)

        if (savedInstanceState!=null) {
            score =  savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else {
            resetGame()
        }




        tapButton.setOnClickListener { _ -> incrementScore() }


    }

    private fun resetGame() {
        score = 0
        timeLeft = 60
        val gameScore = getString(R.string.your_score, Integer.toString(score))
        yourScoreTextView.text = gameScore
        val gameTimeLeft = getString(R.string.time_left, Integer.toString(timeLeft))
        timeTextView.text = gameTimeLeft
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }

            override fun onFinish() {
                endGame()
            }
        }
        gameStarted = false
    }

    private fun incrementScore() {

        score++
        val newScore = getString(R.string.your_score, Integer.toString(score))
        yourScoreTextView.text = newScore

        if (!gameStarted) {
            startGame()
        }

    }

    private fun startGame() {
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame() {
        Toast.makeText(this, getString(R.string.game_over_message, Integer.toString(score)), Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun restoreGame() {


        val restoredScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = restoredScore

        val restoredTime = getString(R.string.time_left, Integer.toString(timeLeft))
        timeTextView.text = restoredTime

        countDownTimer = object : CountDownTimer(timeLeft.toLong() *1000L, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

                timeTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }

            override fun onFinish() {
                endGame()
            }
        }
        countDownTimer.start()
        gameStarted = true

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SCORE_KEY, score)
        outState!!.putInt(TIME_LEFT_KEY, timeLeft)
        countDownTimer.cancel()
        Log.d(TAG, "onSaveInstanceState: score = $score & timeleft = $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy called")
    }


}
